package ro.mta.se.chat.Utils;

/**
 * Created by Marry on 23.10.2015.
 * ClassName: Constants
 * Clasa contine o colectie de constante de utilitate generala
 */
public class Constants {

    public static final int LOGLEVEL = 2;
    public static final boolean LOG_TO_FILE = true; // constanta care imi spune daca fisierele sunt scrise in fisier sau in consola
    public static final String LOG_FILE_NAME = "LogFile.log";
    //nivelul de logging global

}
