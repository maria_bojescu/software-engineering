
/**
 * Created by Marry on 23.10.2015.
 * ClassName: Logger
 * Logger este o clasa pentru logarea mesajelor
 *
 * @author Maria Bojescu
 */

package ro.mta.se.chat.Utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * In clasa Logger am definit nivelele de logging VERBOSE, DEBUG, INFO, WARNING, ERROR, ASSERT
 * in ordinea gravitatii.
 * In fiecare metoda log am comparat nivelul de log global cu nivelul mesajelor ce sunt logate, acesta fiind setat la compilare
 */

public class Logger {

    public static int VERBOSE = 2; //valori predefinite ale nivelelor de log in ordinea gravitatii
    public static int DEBUG = 3;
    public static int INFO = 4;
    public static int WARNING = 5;
    public static int ERROR = 6;
    public static int ASSERT = 7;
    public static long yourmilliseconds = System.currentTimeMillis();
    public static SimpleDateFormat sdf = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
    public static Date resultdate = new Date(yourmilliseconds);
    /**
     *
     * System.currentTimeMillis() - timpul la care a fost afisat mesajul
     */
    //Static Class Reference

    private Logger() {
        /*Constructorul privat va preveni
        * instantierea directa a clasei*/
    }



    public static synchronized void log(int level, String message) {
        if (Constants.LOGLEVEL <= level) {

            String mess = "CURRENT TIME: " + sdf.format(resultdate) + "LOGGING LEVEL: " + level + "MESSAGE: " + message + "\n";

            if (Constants.LOG_TO_FILE) {

                File file = new File(Constants.LOG_FILE_NAME);

                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }


                Path filePath = FileSystems.getDefault().getPath(Constants.LOG_FILE_NAME);//llog e director si const e fisierul
                Charset charset = Charset.forName("US-ASCII");

                try (BufferedWriter writer = Files.newBufferedWriter(filePath, charset, StandardOpenOption.APPEND)) {
                    writer.write(mess, 0, mess.length());
                } catch (IOException x) {
                    System.err.format("IOException: %s%n", x);
                }

            } else {
                System.out.println(mess);
            }
        }
    }

    public static synchronized void log(int level, String message, int errorCode) {
        if (Constants.LOGLEVEL <= level) {

            String mess = "CURRENT TIME: " + sdf.format(resultdate) + "LOGGING LEVEL: " + level + "MESSAGE: " + message + "ERROR CODE: " + errorCode + "\n";

            if (Constants.LOG_TO_FILE) {

                File file = new File(Constants.LOG_FILE_NAME);

                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Path filePath = FileSystems.getDefault().getPath(Constants.LOG_FILE_NAME);//llog e director si const e fisierul
                Charset charset = Charset.forName("US-ASCII");


                try (BufferedWriter writer = Files.newBufferedWriter(filePath, charset, StandardOpenOption.APPEND)) {
                    writer.write(mess, 0, mess.length());
                } catch (IOException x) {
                    System.err.format("IOException: %s%n", x);
                }

            } else {
                System.out.println(mess);
            }
        }
    }

    public static synchronized void log(int level, String message, Exception e) {
        if (Constants.LOGLEVEL <= level) {
            String mess = "CURRENT TIME: " + sdf.format(resultdate) + "LOGGING LEVEL: " + level + "MESSAGE: " + message + "EXCEPTION: " + e + "\n";

            if (Constants.LOG_TO_FILE) {

                File file = new File(Constants.LOG_FILE_NAME);

                try {
                    file.createNewFile();
                } catch (IOException exp) {
                    exp.printStackTrace();
                }


                Path filePath = FileSystems.getDefault().getPath(Constants.LOG_FILE_NAME);//log e director si const e fisierul
                Charset charset = Charset.forName("US-ASCII");

                try (BufferedWriter writer = Files.newBufferedWriter(filePath, charset, StandardOpenOption.APPEND)) {
                    writer.write(mess, 0, mess.length());
                } catch (IOException x) {
                    System.err.format("IOException: %s%n", x);
                }

            } else {
                System.out.println(mess);
            }
        }
    }

    public static synchronized void log(int level, String message, String stackTrace) {
        if (Constants.LOGLEVEL <= level) {
            String mess = "CURRENT TIME: " + sdf.format(resultdate) + "LOGGING LEVEL: " + level + "MESSAGE: " + message + "STACK TRACE: " + stackTrace + "\n";
            if (Constants.LOG_TO_FILE) {


                File file = new File(Constants.LOG_FILE_NAME);

                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }


                Path filePath = FileSystems.getDefault().getPath("logs", Constants.LOG_FILE_NAME);//log e director si const e fisierul
                Charset charset = Charset.forName("US-ASCII");

                try (BufferedWriter writer = Files.newBufferedWriter(filePath, charset, StandardOpenOption.APPEND)) {
                    writer.write(mess, 0, mess.length());
                } catch (IOException x) {
                    System.err.format("IOException: %s%n", x);
                }

            } else {
                System.out.println(mess);
            }
        }
    }
}
