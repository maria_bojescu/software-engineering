
package ro.mta.se.chat.JavaMain;
//sa afisez mesajul de exceptie cu un cod de eroare si in consola, folosind log

import ro.mta.se.chat.Exceptions.ConnectionLostException;
import ro.mta.se.chat.Exceptions.FileNotFoundException;
import ro.mta.se.chat.Exceptions.IllegalAccesException;
import ro.mta.se.chat.Exceptions.InvalidRequestException;
import ro.mta.se.chat.Utils.Logger;

public class Main {

    public static void main(String[] args) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 3; i++) {

                    Logger.log(3, "Message1 " + i);

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        Logger.log(3, "Thread1 error: " + e.getMessage());
                    }
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 3; i++) {
                    Logger.log(3, "Message2 " + i);

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        Logger.log(3, "Thread2 error: " + e.getMessage());
                    }
                }
            }
        }).start();



        /* Obiectul nu poate fi creat direct datorita constructorului privat
         Obiectul va fi creat cu metoda de creare a unui singur obiect*/

        Logger.log(2, "Message Error no.2", 102);
        Exception exeption = new Exception("Mesaage Error no.3");
        Logger.log(1, "Message Error", exeption);


        try {
            throw new ConnectionLostException("Connection could not be established!");
        } catch (ConnectionLostException exp) {
            System.out.println(exp.getMessage());
        }

        try {
            throw new FileNotFoundException("The file could not be found!");
        } catch (FileNotFoundException exp) {
            System.out.println(exp.getMessage());
        }

        try {
            throw new IllegalAccesException("Illegal access!");
        } catch (IllegalAccesException exp) {
            System.out.println(exp.getMessage());
        }

        try {
            throw new InvalidRequestException("Wrong request!");
        } catch (InvalidRequestException exp) {
            System.out.println(exp.getMessage());
        }


    }

}
