package ro.mta.se.chat.Exceptions;

import ro.mta.se.chat.Utils.Logger;

/**
 * Clasa FileNotFoundException este folosita pentru a arunca o exceptie atunci cand calea specificata nu este gasita
 */

public class FileNotFoundException extends Exception {
    String message;

    public FileNotFoundException(String message) {

        this.message = message;
    }

    public String getMessage() {
        Logger.log(6, "The file could not be found!");
        return message;
    }
}
