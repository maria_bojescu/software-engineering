package ro.mta.se.chat.Exceptions;

import ro.mta.se.chat.Utils.Logger;

/**
 * Am creat clasa ConnectionLostException pentru a arunca o exceptie atunci cand nu se poate stabili conexiunea intre client si server.
 */
public class ConnectionLostException extends Exception {
    String message;

    public ConnectionLostException(String message) {
        this.message = message;
    }

    public String getMessage() {
        Logger.log(6, "The connection could not be established!");
        return message;
    }
}
