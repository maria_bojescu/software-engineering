package ro.mta.se.chat.Exceptions;

import ro.mta.se.chat.Utils.Logger;

/**
 * Clasa InvalidRequestException este folosita in momentul in care clientul realizeaza o cerere invalida
 */
public class InvalidRequestException extends Exception {
    String message;

    public InvalidRequestException(String message) {
        this.message = message;
    }

    public String getMessage() {
        Logger.log(6, "Wrong request exception!");
        return message;
    }
}
