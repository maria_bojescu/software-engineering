package ro.mta.se.chat.Exceptions;

import ro.mta.se.chat.Utils.Logger;

public class IllegalAccesException extends Exception {
    String message;

    public IllegalAccesException(String message) {
        this.message = message;
    }


    public String getMessage() {
        Logger.log(6, "You don't have access!");
        return message;
    }
}
